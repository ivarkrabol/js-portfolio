# JS Portfolio

An attempt to create an ascii graphics js framework which can double as my *portfolio*.

## Launch

This doesn't work:

```
docker run \
  --name js-portfolio \
  -v /usr/share/nginx/html:${PATH_TO_PROJECT:-$(pwd)}:ro \
  -p 8080:80 \
  -d nginx
```

Access @ [localhost:8080/index.html](localhost:8080/index.html)

Stop using:


```
docker rm -f js-portfolio
```
